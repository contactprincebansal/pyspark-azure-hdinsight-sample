from pathlib import Path
from sys import argv
import pandas as pd
from pyspark import SparkFiles

from pyspark.sql import SparkSession

if __name__ == "__main__":
    print("=========================== SPARK STARTED ================================")
    app_name = Path(argv[0]).stem
    spark = SparkSession.builder.appName(app_name).getOrCreate()

    try:
        print("=========================== START: USING PANDAS PYTHON PACKAGE ================================")
        csv_url = 'https://www.w3schools.com/python/pandas/data.csv'
        pdf = pd.read_csv(csv_url)
        print(pdf.to_string())
        print(pd.options.display.max_rows)
        print(pdf.tail())
        print(pdf.info())
        print("=========================== END: USING PANDAS PYTHON PACKAGE ================================")

        print("=========================== START: USING SPARK DATAFRAME ================================")
        sc = spark.sparkContext
        sc.addFile(csv_url)
        src = SparkFiles.get('data.csv')
        sdf = spark.read \
            .option("header", True) \
            .format("csv").load(f"file://{src}")
        print(sdf.printSchema())
        print(sdf.count())
        print(sdf.show(truncate=False))
        print("=========================== END: USING SPARK DATAFRAME ================================")
    finally:
        spark.stop()
    print("=========================== SPARK STOPPED ================================")
